import React,{useState,useEffect, useRef} from 'react';
import { BrowserRouter as Router, Link, useLocation  } from "react-router-dom";
import firebase from "firebase/app";
import Header from '../header/header';
import { fire } from "../../firebase";
import "firebase/firestore";
import './Blog.scss';
   
 

let Blog = () => { 

 const [state, setstate] = useState('blog-cogigos');
 const [articles, setarticles] = useState([])
  
 
 useEffect(()=>{
   let getAlldata = [];
    let getFirebaseData = async () => {
         
    try {
        const db = firebase.firestore(fire); 
        let GetAllArticles = await db.collection(state).get(); 
        GetAllArticles.forEach(function(doc) { 
            let datosGet = doc.data(); 
            //    console.log('doc:::',doc.id)
            getAlldata.push({data:datosGet, id:doc.id}) 
        })
    } catch (error) {
        console.log(error);
    }

    setarticles([...getAlldata]) 
};

getFirebaseData();

 
 },[]) 

//   console.log('articles.length',articles)
     const spanBG = { 
          backgroundImage: 'url('+process.env.PUBLIC_URL + 'calendar.png)'
     }
        return (
            <>
              <Header /> 
              <div id="home_body" className="allArticles">
                  <div className="home_body__left">

                     { articles.map(function(doc, i){ 
                         let imgAlt = doc.data.img.split(','); 
                         let FullDate = doc.data.date.toDate().toString();
                         var cuttingData = FullDate.slice(4,10);
                          
                        return (
                        <section key={i} className="wrapper_articles"> 
                        <div className="wrapper_image">
                           <img src={imgAlt[0]} alt={imgAlt[1]} />
                           <a className="readMore__link" href={'/single?id='+doc.id}>Leer el Post...</a> 
                           <span style={spanBG}>{cuttingData}</span>
                           <div className="wrapper__title">   
                               <h1> <Link to={'/single?id='+ doc.id}>{doc.data.title}</Link> </h1>
                           </div>
                           {/* <Child id={query.get("id")} /> */}
                        </div> 
                        </section>
                        )
                        }) 
                     }
                  </div>
                  <div className="home_body__right"></div>
              </div>
            </>
        )
       
     
}

export default Blog
