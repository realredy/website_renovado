import React,{useState} from 'react'  
import Header from '../header/header';
import Titles from '../home_componenets/Titles';
import Printsoftware from '../home_componenets/Printsoftware';
import Webproducer from '../home_componenets/WebProducer';
import Timeline from '../home_componenets/Timeline';
import Portfolios from '../home_componenets/Portfolios';
import About from '../home_componenets/About';
// import Chat from '../chat/Chat';
import Top_section from '../home_componenets/SeccionInicial';
import Top_sectionSlider from '../home_componenets/SectionInitSlider'; 
import TimelineM from '../home_componenets/TimelineMobile';
// import {is_loged} from '../../backend/login/loged';
 

 
function Home() {

const [pantalla, setPantalla] = useState('');
   
 (()=>{
    window.onresize = ()=>{
        document.title = window.innerWidth;
        setPantalla(window.innerWidth); 
    } 
 })();

 useState(()=>{
    setPantalla(window.innerWidth);
    // is_loged.onAuthStateChanged( auth => {
    //      console.log(auth.email);
    //   } ); 
    
},[]);
 
    return ( 
         <> 
        <Header />  
        {pantalla <= 550 ? <Top_sectionSlider /> : <Top_section />    }
            
            <Titles texto="Pints Software" posicion="right" />
            <Printsoftware />
            <Titles texto="web Producer" posicion="left" />
            <Webproducer />
            <Titles texto="Current Time-Line" posicion="right" />
            {pantalla <= 550 ? <TimelineM /> : <Timeline />   }
            <Titles texto="Some Works" posicion="left" />
            <Portfolios />
            <Titles texto="About Me" posicion="right" />
            <About />
            {/* <Chat />  */}
          
        </>
    )
}

export default Home
