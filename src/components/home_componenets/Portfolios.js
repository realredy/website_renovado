 
import React from 'react'
import '../home_componenets/Portfolios.css'; 
import { CarouselProvider, Slider, Slide, DotGroup } from 'pure-react-carousel';



// window.onload = function(){
   
document.addEventListener('click', (e)=>{
     
    if(e.target.classList[0] === 'SelectedTipePortfolios'){
        let allClear = document.getElementsByClassName('SelectedTipePortfolios');
        // allClear.classList.remove('default');
        // e.target.classList.add('default');
        //  console.log(allClear);
         for (let index = 0; index < allClear.length; index++) {
             const element = allClear[index].classList[1];
             let botones = document.querySelector('.'+ element);
            if(botones){
                botones.classList.remove('SelectedPortfolio');
            }
         }
            // let objeto = document.getElementsByClassName(allClear.classList)[0];
               e.target.classList.add('SelectedPortfolio');
        
    }
    
});
// }

function Portfolios() {
   


 

    return (
        <>
            <section id="works" className="boxed">
              <nav id="workSelector">
                  <ul>
                      <li><a onClick={(e)=>{e.preventDefault()}} className="SelectedTipePortfolios SelectedPortfolio">web</a> </li>
                      <li><a onClick={(e)=>{e.preventDefault()}} className="SelectedTipePortfolios">Logotipos</a> </li>
                      <li><a onClick={(e)=>{e.preventDefault()}} className="SelectedTipePortfolios">Print</a> </li>
                      </ul>
                   </nav>
                   <div className="boxShow">
                   <CarouselProvider
        naturalSlideWidth={100}
        naturalSlideHeight={175}
        totalSlides={5}
        infinite={true}
        isIntrinsicHeight={true}
      >
            <Slider>
                <Slide index={0}>
                <div className="wrapperWeb">
                        <img src={process.env.PUBLIC_URL+ 'base-mockup.jpg'} alt="webD" />
                            <section id="innerTextWeb">
                                <ul>
                                    <li><b>Base: </b>wordpress</li>
                                    <li><b>Integraciones: </b>Strype - Paypal - TagManager - Materialize</li>
                                    <div className="wrapperTextWorks">
                                        <p>
                                         LandingPage desarrollado con wp, totalmente personalizado.
                                         En este proyecto se integrado tanto strype como paypal de 
                                         manera integrada por lo que todo el proceso es realizado 
                                         sin redirecciones. Dentro de las cualidades de este portal
                                         está la capacidad de poder insertar información dentro de 
                                         la base de datos en diferentes momentos del proceso, como en 
                                         diferentes secciones y con procesos tán complejos como: 
                                        insertar multimedia, crear post, insertar múltiples post_meta,
                                        crear usuarios. Otra capacidad es la de envio de emails autumatizados,
                                        además de acceder a los datos suministrados previamente mediante
                                        un sistema de login.
                                        Theme 100% programado desde cero, 100% responciv.
                                        </p>
                                    </div>
                                </ul>
                            </section>
                       </div>
                </Slide>
                <Slide index={1}>
                <div className="wrapperWeb">
                        <img src={process.env.PUBLIC_URL+ 'base-mockup2.jpg'} alt="webD" />
                            <section id="innerTextWeb">
                                <ul>
                                    <li><b>Base: </b>HTML - CSS - JavaScript</li>
                                    <li><b>Integraciones: </b>WoW.js - Materialize</li>
                                    <div className="wrapperTextWorks">
                                        <p>
                                         En este portal, sencillo o de finalidad informativa, ( en este
                                         solo participo realizando el maquetado y la funcionalidad ),
                                         quedando el concepto de diseño en mano de un tercero. Solo 
                                         me compete mecionar el tiempo de desarrollo, 7 días x 4 horas. 
                                         Y es en lo que destaco, en la manera o técnicas que implemento 
                                         para desarrollar sitios en corto tiempo, claro una vez tengo 
                                         todo a la mano como diseño, imágenes y recursos.
                                        </p>
                                    </div>
                                </ul>
                            </section>
                       </div>
                </Slide>
                <Slide index={2}>
                <div className="wrapperWeb">
                <div id="wrapperVideo">
                <video width="200" controls>
                    <source src={process.env.PUBLIC_URL+ "./media/mobileApp.mp4"} type="video/mp4" /> 
                    Your browser does not support HTML video.
                    </video>
                    </div>
                            <section id="innerTextWeb">
                                <ul>
                                    <li><b>Base: </b>Angular - Ionic:3</li>
                                    <li><b>Integraciones: </b>Materialize</li>
                                    <div className="wrapperTextWorks">
                                        <p>
                                         Esta all (showroom) creada con la finalidad de mostar  los 
                                         articulos de un woocomerce. Alimentada de una api creada con 
                                         php la cual genera un JSON, cuenta con la posibilidad de compartir
                                         el contenido en redes sociales. y su propio backend para agregar nuevos
                                         productos, editar y eliminar.
                                        </p>
                                    </div>
                                </ul>
                            </section>
                            
                       </div>
                </Slide>
                <Slide index={3}>
                <div className="wrapperWeb">
                <div id="wrapperVideo">
                 
                    <img src={process.env.PUBLIC_URL+ 'base_mockup_2.jpg'} alt="web_master" />
                      
                    </div>
                            <section id="innerTextWeb">
                                <ul>
                                    <li><b>Base: </b>Wordpress</li>
                                    <li><b>Integraciones: </b>PHP - Elementor - Javascript</li>
                                    <div className="wrapperTextWorks">
                                        <p>
                                         Web site con alto sentido del diseño, colores, íconos, control de espacios
                                         creada totalmente con elementor pero con algunas implementaciones de javascript
                                         para darle funcionalidad a algunas secciones.
                                        </p>
                                    </div>
                                </ul>
                            </section>
                            
                       </div>
                </Slide>

                <Slide index={4}>
                <div className="wrapperWeb">
                <div id="wrapperVideo">
                 
                    <img src={process.env.PUBLIC_URL+ 'landing_page.jpg'} alt="web_master" />
                      
                    </div>
                            <section id="innerTextWeb">
                                <ul>
                                    <li><b>Base: </b>PHP - MySQL</li>
                                    <li><b>Integraciones: </b>Semantic UI</li>
                                    <div className="wrapperTextWorks">
                                        <p>
                                         Este landingpage o formulario, desarrollado 100% con php-javascript-materialUI con más de 
                                         4 capas de seguridad, total seguimiento de las KeyCaps para controlar los datos suministrados
                                         y a la vez evitar los bots, envio de datos hacia el c*** para la cual se suma otra integración extra.
                                        </p>
                                    </div>
                                </ul>
                            </section>
                            
                       </div>
                </Slide>
            </Slider>
            <DotGroup />  
     </CarouselProvider>

                        
                   </div>
            </section> 
        </>
    )
}

export default Portfolios;

