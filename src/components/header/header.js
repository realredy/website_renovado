import React,{useLayoutEffect}  from 'react' 
import MetaTags from 'react-meta-tags';
import {
    BrowserRouter as Router, 
    Link
  } from "react-router-dom";

 
     
     
      

function Header() {

    useLayoutEffect(()=>{ 
        let originUrl = window.location.pathname.split('/');  
          document.getElementsByTagName('body')[0].id =  `${originUrl[1]}`;

        setTimeout(()=>{
            switch (originUrl[1]) {
                case '':
                    return(
                    <MetaTags> 
                    <meta property="og:type" content="website" />  
                    <meta property="og:title" content="Portfolios digital Ricardo Perez Lavour" /> 
                    <meta property="og:description" content="Site build for Ricardo Perez Lavour, reactJS technology, firebase, webpack" /> 
                    <meta property="og:image" content="https://lavour.es/share_img.jpg"/> 
                    <meta property="og:url" content="https:lavour.es" />  
                    <meta property="og:site_name" content="Web Page Ricardo P. Lavour" />
                    </MetaTags>
                    )
                    break;
                    case 'blog':
                        return(
                    <MetaTags> 
                    <meta property="og:type" content="website" />  
                    <meta property="og:title" content="Blog Ricardo perez website" /> 
                    <meta property="og:description" content="Inofo about all oun experience in like web developer" /> 
                    <meta property="og:image" content="https://lavour.es/share_img.jpg"/> 
                    <meta property="og:url" content="https:lavour.es/blog" />  
                    <meta property="og:site_name" content="Web Page Ricardo P. Lavour" />
                    </MetaTags>
                    )
                    break;
                    case 'single':
                        let img = document.querySelector('.wrapper_single img');
                        let title = document.querySelector('.single_title');
                        
                        return(
                    <MetaTags> 
                    <meta property="og:type" content="website" />  
                    <meta property="og:title" content={title.textContent} /> 
                    <meta property="og:description" content="Read more information abour this topic" /> 
                    <meta property="og:image" content={img.src}/> 
                    <meta property="og:url" content={window.location.href} />  
                    <meta property="og:site_name" content="Web Page Ricardo P. Lavour" />
                    </MetaTags>
                    )
                    break;
            
                default:
                    break;
            }
        },2000)
            
     

    // console.log(document.getElementsByTagName('body')[0]);
    },[])
    return (
        <>
         
         <header> 
            <div id="innerHeader" className="boxed"> 
            <a href="/"><img src={process.env.PUBLIC_URL+ 'lavour_logo.png'} alt="logo lavour"/></a>
             
            <ul id="micromenu">
                    <li> <Link to="/blog">Vamos a mi blog</Link> </li> 
                    <li>Sobre Maquetación</li> 
                    <li>Que tanto de php y MySQL</li> 
                    <li>Que tanto de JavaScript</li> 
                    <li>..Y Aplicaciones Moviles</li> 
                    <li>Quieres Contactarme</li> 
                </ul>
            </div>
            
         </header>
        </>
    )
}

export default Header
