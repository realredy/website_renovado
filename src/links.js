import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Home from './components/home/Home';
import Login from './backend/login/login';
import Home_admin from './backend/home_admin/Home_admin'; 
import Blog from './components/blogComponenent/blog'  
import Single from './backend/single/Single';
import '../src/index.css';


 
function Links() {
              return ( 
                <BrowserRouter> 
                  <Route exact path="/" component={Home} />  
                        <Route path="/backend/admin" component={Home_admin} /> 
                        <Route path="/backend/login" component={Login} /> 
                        <Route path="/blog" component={Blog} /> 
                        <Route path="/Single" component={Single} /> 
                </BrowserRouter> 
               )
}
export default Links
