import React from 'react';
import ReactDOM from 'react-dom'; 
import Links from './links'; 
import * as ServiceWorker from './serviceWorker';
 //import './index.css';

// ReactDOM.render(
//   <React.StrictMode>
//     <Links />
//   </React.StrictMode>,
//   document.getElementById('root')
// );


const rootElement = document.getElementById("root");

if (rootElement.hasChildNodes()) {
  ReactDOM.hydrate(<Links />, rootElement);
} else {
  ReactDOM.render(<Links />, rootElement);
}
 

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
ServiceWorker.unregister();
