import React from "react";
//=============== EDITOR TINYIMCE ===================//
import "tinymce/tinymce";
import "tinymce/icons/default";
import "tinymce/themes/silver";
import "tinymce/plugins/paste";
import "tinymce/plugins/link";
import "tinymce/plugins/image";
import "tinymce/plugins/table";
import "tinymce/skins/ui/oxide/skin.min.css";
import "tinymce/skins/ui/oxide/content.min.css";
import "tinymce/skins/content/default/content.min.css";
import { Editor } from "@tinymce/tinymce-react";
//=============== EDITOR TINYIMCE ===================//
import "./All.css";

import firebase from "firebase/app";
import "firebase/firestore";
import { fire } from "../../firebase";
import { env } from "process";
// create new fech


let textoHTML ;

let handleEditorChange = (e) => {
   textoHTML = e;
};

let actionBox = (e) => {
  switch (e.target.nodeName) {
    case "IMG":
      let hiddendataImage = document.getElementById("imageURL");
      let selectedImage = document.getElementById("selectedImage");
      hiddendataImage.value = `${e.target.src},${e.target.alt}`;
      selectedImage.src = e.target.src;
      break;
    case "SPAN":
      let close = document.getElementById("gallery_selector");
      close.classList.remove("show_image_selector");
      break;

    default:
      break;
  }
};
let getImage_new = (image) => {
  var IMG_reader = new FileReader();
  IMG_reader.onload = function () {
    let metadata_img = IMG_reader.result;
    var output = document.getElementById("selectedImage");
    output.src = metadata_img;
  };
  IMG_reader.readAsDataURL(image.target.files[0]);
};

//==============================================================================

let showList_ofImage = (e) => {
  e.preventDefault();
  var activador = false;
  var close =
    e.target.parentNode.parentNode.parentNode.children[0].children[0]
      .children[0];

  close.addEventListener("click", () => {
    selectorImagen.classList.remove("show_image_selector");
    activador = !activador;
  });

  let selectorImagen = document.getElementById("gallery_selector");
  if (activador === false) {
    selectorImagen.classList.add("show_image_selector");
    activador = !activador;
  }
};

let saveData = (e) => {
  e.preventDefault(); 
  let title = document.getElementById('article_title').value; 
  let imagen = document.getElementById('imageURL').value;
  let selector = document.getElementById('categoria').value;
  let text = textoHTML;
    if('' !== title && '' !== text && '' !== imagen && '' !==  selector){
      
      sendata_to_firebase(selector, title, text, imagen); 
    } else {
      alert('no puede continuar!!!')
    }
}


 let sendata_to_firebase  = async (selector, title, text, imagem) => {
   let form01 = document.getElementById('form01');
   let form02 = document.getElementById('form02'); 
   let df_img = document.getElementById('selectedImage');
   let defaultIMG = process.env.PUBLIC_URL + '../../defaultArticle.jpg';
   let submitBTN = document.getElementById('submitBTN');
  let datos = {
    title: title,
    text: text,
    img: imagem,
    date:firebase.firestore.Timestamp.fromDate(new Date())
  }
  try{   
    const db  =  firebase.firestore(fire);
    let getDatab = db.collection('blog-'+selector).doc(); 
    const env = await getDatab.set(datos,{merge:true}).then((e)=>{
      form01.reset();
      form02.reset();
      df_img.src = defaultIMG;
            
      submitBTN.value = "Enviado";
      submitBTN.style.backgroundColor = "#185b13";

      setTimeout(()=>{
        submitBTN.value = "Guardar el articulo";
      submitBTN.style.backgroundColor = "#d10808";
      },5000)
    }) 

} catch (error){
    console.log("Resultados de un error al guardar: ",error) 
} 
}

class All extends React.Component {
  constructor(props) {
    super(props);
    this.state = "";
    this.boxCaja = React.createRef();
  }

 

  componentDidMount() {
    let getFirebaseData = async () => {
      let boxImg = document.getElementsByClassName("wrapper_gallery")[0];
      try {
        await firebase
          .storage(fire)
          .ref("all/")
          .listAll()
          .then((e) => {
            e.items.map((a) => {
              a.getDownloadURL().then((f) => {
                a.getMetadata().then((e) => {
                  boxImg.innerHTML +=
                    `<img width="70px" src=` +
                    f +
                    ` alt=` +
                    e.customMetadata.alt +
                    ` />`;
                  // fillData.push(f);
                });
              });
            });
          });
      } catch (error) {
        console.log(error);
      }
    };
    getFirebaseData();
  }


  render() {
    return (
      <>
        <div id="AllArticlewrapper">
          <div id="gallery_selector">
            <div
              ref={this.boxCaja}
              onClick={actionBox}
              className="wrapper_gallery"
            >
              <span className="closeGallery">+</span>
            </div>
          </div>

          <div className="AllArticlewrapper_head ">
            <span>Seccion para anadir nuevos articulos</span>
          </div>
          <div className="AllArticlewrapper_leftColumn">
            <form id="form01" action="post">
              <label htmlFor="article_title">Title article_title</label>
              <input id="article_title" type="text" name="title" />
              <Editor
                //  initialValue="<p>This is the initial content of the editor</p>"
                outputFormat='text/html'
                tagName='section'
                
                init={{
                  skin: false,
                  content_css: false,
                  height: 200,
                  menubar: true, 
                  plugins: ["link image", "table paste"],
                  toolbar:
                    "undo redo | image | formatselect | bold italic backcolor | \
           alignleft aligncenter alignright alignjustify | \
           bullist numlist outdent indent | removeformat | help",
                }}
                onEditorChange={handleEditorChange}
              />
              <input id="submitBTN" onClick={saveData} type="submit" value="Guardar el articulo" />
            </form>
          </div>
          <div className="AllArticlewrapper_rightColumn">
            <form id="form02" action="">
              <img src={process.env.PUBLIC_URL + '../../defaultArticle.jpg'} id="selectedImage" width="100%" alt="lavour page" />
              <label htmlFor="image_selector">Select new image</label>
              <input
                onChange={getImage_new}
                type="file"
                name="image"
                id="image_selector"
              />
              <input id="imageURL" type="hidden" name="imageURL" />
              <button onClick={showList_ofImage}>Imagen de la galeria</button>
              <label htmlFor="categoria">Seleccione la categoria</label>
              <select name="categ" id="categoria">
              <option>-------------</option>
                <option value="cogigos">codigos</option>
                <option value="ejemplos">ejemplos</option>
                <option value="optimizacion">optimizacion</option>
              </select>
            </form>
          </div>
        </div>
      </>
    );
  }
}
export default All;
