import React from 'react';
import './login.scss'; 
import firebase from 'firebase';
import {fire} from '../../firebase'; 

function Login() { 
        function formMakeLoguin(e){ 
        let msgBody = document.getElementsByClassName('loguinForm__wrrapper-error-mensaje')[0];
        let errMsg = document.getElementById('loguinForm__errorMessage');
                e.preventDefault();
            let mail = e.target.mail.value;
            let pass = e.target.pass.value;
        firebase.auth(fire).setPersistence(firebase.auth.Auth.Persistence.SESSION)
            .then(()=>{
              return firebase.auth(fire).signInWithEmailAndPassword(mail, pass)
            .then(user=>{ 
                 if( user ){ 
                 window.location.href="/backend/admin";
              }
            })
        }).catch(err=>{
            console.log( err.message )
            msgBody.style.display="inherit";
                errMsg.innerHTML = err.message;
                setTimeout(()=>{
                msgBody.style.display="none";
            }, 7000 ) 
            })
       }
      const backSt = { backgroundImage: `url('../../base_login.jpg')`};
    return (
        <div id="loguinForm" style={backSt} >
                <div className="loguinForm__BoxWrapper">
                <a href="/"><img src={process.env.PUBLIC_URL +'../../lavour_logo.png'} alt="error Mensaje" /></a>
                  <div className="loguinForm__wrapper"> 
                    <span className="loguinForm__title">
                      Coloque su email y password
                    </span>
                  <form onSubmit={formMakeLoguin}>
                    <input id="nombre" type="email" name="mail" placeholder="Nombre" />
                    <input id="pass" type="password" name="pass" placeholder="**********" /> 
                    <input type="submit" value="Loguin" />
                    <div className="loguinForm__wrrapper-error-mensaje"> 
                     <span id="loguinForm__errorMessage"></span>
                    </div>
                  </form>
            </div>
          </div>
        </div>
    )
 }

export default Login;
