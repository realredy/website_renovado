import React,{useEffect, useState} from 'react';
import StatusBar from '../statusBar/StatusBar';
import All from '../allArticles/All';
import Media from '../media/Media'
import {is_loged} from '../login/loged';
import './homeadmin.css';  
 
 
const Home_admin = () => { 
const [menuk, setmenuk] = useState(<All />)
    let menu_select = (menutype)=>{  
      switch (menutype.target.id) {
        case 'all':
          setmenuk( <All />);
          break;
          case 'media':
          setmenuk( <Media />);
          break;
          case 'new':
            setmenuk(<p>No menu</p>);
            break; 
            default:
             
              break;
      }  
    }
  useEffect(()=>{ 
    is_loged.onAuthStateChanged( auth => { 
        if( auth === null){ 
            window.location.href="/backend/login";
          }  
       });
},[]);




                  return (
                    <>
                    <StatusBar />
                    <main id="Backend">
                        <div className="spacer"></div>
                        <div className="Backend__wrapperBody">
                      <section className="Backend__leftcontroller">
                          <div>
                          <ul>
                            <li id="all" onClick={menu_select} className="Backend__hassChild">
                              Articulos
                                <ul>
                                    <li id="new" onClick={menu_select} >anadir nuevo</li>
                                    <li>nueva categoria</li>
                                    </ul>
                              </li>
                              <li id="media" onClick={menu_select} >Multimedia</li>
                            <li>Maquetacion</li>
                            <li>JavaScript</li>
                            <li>PhP&MySQL</li>
                            <li>Mobile app</li>
                          </ul>
                        </div>
                      </section>
                      <section className="Backend__body">
                          <div className="Backend__body-wrapper">
                              {menuk}
                            </div>  
                          </section>
                      </div>
                    </main>
                    </>
                )


     
  

   

}


export default Home_admin;