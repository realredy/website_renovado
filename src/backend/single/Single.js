import React,{useEffect, useState, useLayoutEffect} from 'react';
import firebase from "firebase/app";
import Header from '../../components/header/header';
import "firebase/firestore";
import { fire } from "../../firebase";
import './Single.scss';
//=====================================//

 
 

let getUrl = ()=>{ 
let getURL = window.location.href; 
 const URL = getURL.split('=')[1]; 
return [URL]; 
}
 

function Single() {    

   const [single_article, setsingle_article] = useState([]);


    useEffect(() => { 
            
    }, []);


    useLayoutEffect(() => {
        let getSingle__article = async () => { 
            try {
                const db = firebase.firestore(fire); 
                let GetAllArticles = await db.collection('blog-cogigos').doc(getUrl()[0]).get();  
                let data = GetAllArticles.data(); 
                  let imgs = data.img.split(',')[0];
                  let title = data.title;
                  let text = data.text;  
                  let years = data.date.toDate().toString().slice(10,15);
                  let Date = data.date.toDate().toString().slice(4,11);

                     document.title = title;
                     setsingle_article([imgs, title, years,Date, text])   
                     
            } catch (error) {
                console.log(error);
            } 
        };
        
        getSingle__article();
 
        // window.onload = ()=>{
                setTimeout(()=>{
                if(document.getElementById('convertText')){
                  let div = document.getElementById('convertText');
                        let conv = div.innerText.toString();
                     div.innerHTML = conv;   
                } 
               },2000)
    }, [])

    //   let imgAlt = single_article.img.split(','); 
    // let FullDate = single_article.date.toDate().toString(); 
    //  console.log(single_article[1])
    return (
        <>
         <Header />   
                <main id="main_single">
                    <section className="wrapper_single">  
                    
                        <img src={single_article[0]}  />
                        <div className="wrapperData">
                        <span> {single_article[3]} </span> 
                        <b> {single_article[2]} </b>  
                        </div> 
                            <h1 className="single_title">{single_article[1]}</h1>
                            <div id="convertText">{single_article[4]}</div> 
                    </section>
                </main> 
        </>
    )
}

export default Single;