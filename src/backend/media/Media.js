import React from 'react';
import firebase from 'firebase/app';
import {fire} from '../../firebase'; 
// import Uploader from './uploader';


    // guardar en una variable la seccion de guardado
//const db = firebase.storage(fire);
    // ruta nombre de la ruta donde sera guardada la imagen
    // ej: artuculos0052-4/nombre.jpg
    // nombew del archivo y la extencion
//const storag = db.ref(ruta+'/'+ nombre+'.'+extencion);


// ---------------- pre visualiuzador
let preview_image = (e) =>{ 
    let image = e.target.files[0];
var IMG_reader = new FileReader(); 
IMG_reader.onload = function(){
let metadata_img = IMG_reader.result;
    var output = document.getElementById('selectedImage_media');
    output.src =  metadata_img;  
 };
  IMG_reader.readAsDataURL(image);
  return e;
// ---------------- pre visualiuzador
}

let save_image = (e) => {   
    e.preventDefault();
    let image = e.target[1].files[0];
    let image_type = e.target[1].files[0].type;
    let image_name = e.target[1].files[0].name;
    let title = e.target[0].value; 
    //    console.log( Uploader(image,image_type,title,firebase,fire) ); 
    let extDividida = image_type.split('/')[1]; 
    const db = firebase.storage(fire);
    const storag =  db.ref('all/'+ image_name+'.'+extDividida);
    const metadata = {
        customMetadata: {
          'alt': title
        }
      }
      
    const subio = storag.put(image, metadata);
 subio.on('state_changed', function(snapshot){ 
        // ==== console.log(snapshot) === //
        }, function(error){
            console.log("Error al subir imagen",error);
        }, function(){
        db.ref('all/').child(image_name+'.'+extDividida).getDownloadURL()
        .then( ur=>{ 
        console.log(ur) 
        })
    })
}
 
 
export default function Media() {
    return (
        <>
             <div id="AllArticlewrapper">
            <div className="AllArticlewrapper_head "><span>Seccion para Subir archivo multimedia</span> </div> 
             <div className="AllArticlewrapper_leftColumn">
                  <form onSubmit={save_image}>
                      <label htmlFor="article_title">Texto Alt para la imagen</label>   
                      <input id="article_title" type="text" name="title" />
                      <label htmlFor="image_selector">Select new image</label>
                      <input onChange={preview_image} type="file" name="image" id="image_selector" />
                       
                     <input type="submit" value="Guardar el articulo" />
                  </form>
             </div>
             <div className="AllArticlewrapper_rightColumn">
                 <form action=""> 
                 <img src="" id="selectedImage_media" width="100%" alt="lavour web page" />  
                   <label htmlFor="categoria">Pre visualizador de la imagen</label> 
                 </form>
             </div>
             </div>  
        </>
    )
}
